import 'dart:html';
import 'dart:js_util';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Iniciando Flutter',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class MyHomePage extends StatelessWidget {
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    Widget child;
    switch (_index) {
      case 0:
        child = FlutterLogo();
        break;
      case 1:
        child = MaterialApp(
            debugShowCheckedModeBanner: false, home: new SecondPage());
        break;
      case 2:
        child = MaterialApp(
            debugShowCheckedModeBanner: false, home: new ThirdPage());
        break;
    }
    return Scaffold(
      body: SizedBox.expand(child: child),
      bottomNavigationBar: BottomNavigationBar(
          onTap: (newIndex) => setState(() => _index = newIndex),
          currentIndex: _index,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home, color: Colors.blueAccent, size: 30.0),
                title: Text("Home")),
            BottomNavigationBarItem(
                icon: Icon(Icons.home, color: Colors.blueAccent, size: 30.0),
                title: const Text("Registro")),
            BottomNavigationBarItem(
                icon: Icon(Icons.home, color: Colors.blueAccent, size: 30.0),
                title: Text("Home")),
          ]),
    );
  }
}
